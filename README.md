# fyd

> FYD client-side Vue project

## Build Setup

We make use of a[Docker](https://www.docker.com/)development environment in order to keep the project and its dependencies ready to work. 

``` bash
# Just build it for the first time or with Docker changes 
docker-compose build

# Get it up
docker-compose up
```

Open `http://localhost:8080` in your web browser and start coding!!

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

#### Issues
* Hot reloading doesn't work, you need to reload the page manually
