FROM node:8

# Create app directory
WORKDIR /usr/src/app
COPY package*.json ./

ENV HOST 0.0.0.0
ENV PORT 8080
ENV NODE_ENV development

RUN npm install
COPY . .

EXPOSE 8080

CMD [ "npm", "start" ]
